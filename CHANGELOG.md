<!--
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Changelog


## 1.2.3 - 2022-11-17

### Added

- Brazilian Portuguese translation by @rafaelff
- Spanish translation by @dariasteam


## 1.2.2 - 2022-11-03

### Fixed

- Some tooltips were missing


## 1.2.1 - 2022-10-15

### Fixed

- Gap when looping Zaps


## 1.2.0 - 2022-10-15

### Added

- Sample Zaps in the default collection (suggested by @cassidyjames and @hadess1)
- Dutch translation by @philip.goto

### Changed

- Updated French translation
- Updated German translation by @gastornis

### Fixed

- Zaps were not removed when removing a collection
- Fading out was not clamped to the end of the playing Zap


## 1.1.0 - 2022-10-06

### Added

- Ability to stop playing with a fade out
- German translation by @gastornis

### Fixed

- The default collection was not created
- Wrong application name in the About window


## 1.0.0 - 2022-09-30

Initial release.
